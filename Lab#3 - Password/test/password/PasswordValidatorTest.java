package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Edmund Leung 991531421
 */

public class PasswordValidatorTest {
	
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid length is in password", PasswordValidator.isValidLength("12345667890"));
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid length is in password", PasswordValidator.isValidLength(null));
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid length is in password", PasswordValidator.isValidLength("          "));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid length is in password", PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid length is in password", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testHasEnoughDigitsRegular() {
		assertTrue("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("12345678910"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Invalid number of digits in password", PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryIn() {
		assertTrue("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("password12"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Invalid number of digits in password", PasswordValidator.hasEnoughDigits("password2"));
	}
}
