package password;

/*
 * @author Edmund Leung 991531421
 * 
 * This class validates passwords and it will be developed using TDD
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;
	
	
	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasEnoughDigits(String password) {
		if (password != null && password.trim().length() >= MIN_LENGTH) {
			int numCount = 0;
			for (int i = 0 ; i < password.length(); i++) {
				char charPosition = password.charAt(i);
				if (charPosition >= '0' && charPosition <= '9') {
					numCount++;
				}
			}
			return (numCount >= MIN_NUM_DIGITS);
		}
		return false;
	}
}
